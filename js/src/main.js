/*global enquire, $, YT, viewportSize, Froogaloop, Hammer, jl*/

console.log('TEST SCRIPTiiiiigiukylehouston');




var isWork = window.is_work,
    isHome = window.is_homepage,
    isProject = window.is_project,
    isPresentation = window.is_presentation,
    isAbout = window.is_about,
    isPreview = window.is_preview,
    // isPeople = window.is_people,
    isMobile,
    bgImgPath,
    aboutImg;

    //Check what mode we are in and set a global var isMobile;
    enquire.register('screen and (max-width:640px)', {
        match : function() {
            isMobile = true;
            bgImgPath = window.imgSmall;
            aboutImg = window.aboutImgSmall;
        }
    }).register('screen and (min-width:641px) and (max-width:1399px)', {
        match : function() {
            isMobile = false;
            bgImgPath = window.imgMedium;
            aboutImg = window.aboutImgMedium;
        }
    }).register('screen and (min-width:1400px)', {
        match : function() {
            isMobile = false;
            bgImgPath = window.imgLarge;
            aboutImg = window.aboutImgLarge;
        }
    });

//Shim for request animation frame
var vendors = ['ms', 'moz', 'webkit', 'o'];
for(var i = 0; i < vendors.length && !window.requestAnimationFrame; ++i) {
    window.requestAnimationFrame = window[vendors[i]+'RequestAnimationFrame'];
}


(function () {
    "use strict";

var JL = {
    /**
     *  Quickly checks the device pixel ratio
     */
    getDevicePixelRatio: function() {
        if (window.devicePixelRatio === undefined) {
            return 1;
        }
        return window.devicePixelRatio;
    },
    /**
     *  Browser Detection
     */
    detectBrowser: function() {

        var this_ = this;

        $.browser = {};
        $.browser.webkit = /webkit/.test(navigator.userAgent.toLowerCase());
        this_.selectors.animationSelector = ($.browser.webkit === true) ? $('body') : $('html');
        return this_.selectors.animationSelector;
    },
    /**
     *  Method simply sets the width and height of the element passed in
     *  @el = selector @width = int @height = int
     */
    setRelativeUnits: function (el, width, height) {

        el.css({
            width: width,
            height: height
        });
    },
    /**
     *  Process first of all performs a check to see whether or not devices pixel density of > 1
     *  If the device density is > 1 then we set our datatypes to be used when looping through our targeted
     *  images which are .img-swap so mobile + hdpi == 'imgmedium' or notmobile + hdpi == 'imglarge'
     *  IF the device fails the devicePixelRatio test ( is < 1 ) then we check if its mobile and if so just return to break processing
     */
    processImages: function ( mode ) {

        var this_ = this,
            dataType,
            thisImage,
            imageHdpi,
            images = $( '.img-swap' );

        if ( this_.getDevicePixelRatio() > 1 ) {
            //If mode == mobile
            if( mode ) {
                dataType = 'imgmedium';
            } else { //else run this
                dataType = 'imglarge';
            }

        } else {

            if( mode ) {
                return;
            } else {
                dataType = 'imgmedium';
            }

        }


        for ( var i = 0, len = images.length; i < len; i++ ) {
            thisImage = $( images[i] );
            imageHdpi = thisImage.data( dataType );
            thisImage.attr( 'src', imageHdpi );
        }

    },
    processContentImages: function(){
        if(isMobile){
            return;
        }

        var images = $('.content-image-swap');

        images.each(function(index, element){
            var $el = $(element).children('img');
            $el.attr('src', $el.data('highres'));
        });
    },

    /**
     *  Set up all scrollto animations for main nav
     *
     */
    configureNavigation: function () {

        var this_ = this;

        if( isHome ) {
            $('.to-top').off().on('click', function (e) {
                e.preventDefault();

                if (!this_.flags.isScrolling) {
                    this_.flags.isScrolling = true;

                    this_.stored.animationSelector.animate({
                        scrollTop: 0
                    }, 1350, 'swing', function() {
                        this_.flags.isScrolling = false;
                    });
                }
            });
        }

        if(!Modernizr.touch){
            this_.selectors.mainNavLinks.off().on('click', function(e){
                if(isHome && e.currentTarget.attributes.href.value.indexOf('#') > -1){
                    e.preventDefault();
                    this_.navScroll(e.currentTarget);
                    this_.selectors.mainHeader.toggleClass('is-active');
                }
            });
        }else{
            this_.selectors.mainNavLinks.off().on('touchend', function(e){
                e.stopPropagation();
                if(isHome && e.currentTarget.attributes.href.value.indexOf('#') > -1){
                    e.preventDefault();
                    this_.navScroll(e.currentTarget);
                    this_.selectors.mainHeader.toggleClass('is-active');
                }
            });
        }
    },
    navScroll: function ( element ) {

        var this_ = this;

        var el = $(element),
                infoSection = el.attr('href');

            if (!this_.flags.isScrolling) {
                this_.flags.isScrolling = true;
                el.siblings().removeClass('is-active').end().addClass('is-active');

                this_.stored.animationSelector.animate({
                    scrollTop: $(infoSection).offset().top - this_.stored.mainHeaderHeight
                }, 1350, 'swing', function(){

                    if( isMobile ) {
                        this_.selectors.mainNav.removeClass('is-active');
                        this_.selectors.mobNavToggle.removeClass('is-active');
                    }

                    this_.flags.isScrolling = false;
                });
            }
    },
    /**
     *  On scroll fade main navigation background opacity from 0 to 1
     */
    animateNav: function () {
        var headerBg = $('.header-bg');


        if( $(window).scrollTop() >= 100 ) {
            headerBg.addClass('is-active');
        }

        $(window).scroll( function () {

            if($(window).scrollTop() >= 100 ){
                headerBg.addClass('is-active');

            }

        });

    },
    initiateParallax: function () {

        if(!window.requestAnimationFrame){
            $('.stellar-bg').css('background-attachment', 'scroll');
            return;
        }

        $.stellar({
            horizontalScrolling: false,
            parallaxBackgrounds: true,
            parallaxElements: false,
            responsive: true,
            verticalOffset: 0,
            horizontalOffset: 0
        });

    },
    /**
     *  Client Logo Carousel
     */
    logoFader: function () {
        //$('.logo-fader > li:gt(0)').hide();
        var logos = $('.logo-fader > div'),
            logosLen = logos.length,
            logosClass = 'show-logo',
            i = 0;
        $(logos[0]).addClass(logosClass);
        setInterval(function(){
            $(logos[i]).removeClass(logosClass);
            if (i === (logosLen -1)) {
                $(logos[0]).addClass(logosClass);
                i = 0;
            } else {
                $(logos[i+1]).addClass(logosClass);
                i++;
            }
        },2000);
    },
    /**
     *  Initialise Grid - this function has excaped me slightly
     *  function now does more than initialise grid, grid gets initialised and grid items are faded in
     *  if on project page deferred objects are set up that monitor image load etc before displaying items on page
     */
    initialiseGrid: function () {

        var this_ = this;

        if( isWork ) {
            this_.selectors.grid.imagesLoaded(function () {
                this_.selectors.grid.packery({
                    itemSelector: '.m-grid-item'
                });

                this_.selectors.grid.packery('bindResize');

                this_.selectors.preloader.removeClass('is-active');

                this_.selectors.grid.addClass('is-active');
            });
        }

        if( isProject || isAbout || isPreview ) {
            var def1 = $.Deferred(),
                def2 = $.Deferred(),
                def3 = $.Deferred(),
                def = $.when(def1, def2, def3);


            $('.img-load').attr('src', bgImgPath);


            this_.selectors.grid.imagesLoaded(function () {
                this_.selectors.grid.packery({
                    itemSelector: '.m-grid-item'
                });

                this_.selectors.grid.packery('bindResize');
                def1.resolve();
            });

            $('.section-ctn').imagesLoaded(function () {
                def2.resolve();
            });

            $('.img-load').imagesLoaded(function () {
                def3.resolve();
            });


            def.done(function () {
                this_.selectors.preloader.removeClass('is-active');

                setTimeout(function () {
                    $('.fader').addClass('is-active');
                    $('.indiv-project-overlay').addClass('fader');

                    this_.displayProjectImages();
                }, 500);

                setTimeout(function () {
                    $.publish('animationComplete');
                }, 1200);
            });
        }

        if( isPresentation ) {
            var introImageDef = $.Deferred(),
                presentationImagesDef = $.Deferred(),
                presentationImages = $.when(introImageDef, presentationImagesDef);

            $('.presentation-intro').imagesLoaded(function () {
                introImageDef.resolve();
            });

            $('.presentation-image').imagesLoaded(function () {
                presentationImagesDef.resolve();
            });

            presentationImages.done(function(){
                this_.selectors.preloader.removeClass('is-active');
            });
        }

    },
    /**
     *  Run image load check on intro image
     *  on deffered done
     */
    preloadBackgroundImage: function () {

        var this_ = this,
            def1 = $.Deferred(),
            def2 = $.Deferred(),
            def = $.when(def1, def2);

        if( window.location.hash ) {
            $('html').removeClass('is-disabled');
            //this_.selectors.preloader.hide();
            this_.selectors.introSection.addClass('is-active');
            $('.intro-logo-ctn').addClass('is-active');

            setTimeout(function () {
                this_.selectors.preloader.removeClass('is-active');
            }, 500);

        } else {
            $('html').addClass('is-disabled');

            $('.img-load').attr('src', bgImgPath).imagesLoaded(function () {
                def1.resolve();
            });

            $('.intro-logo-ctn').imagesLoaded(function () {
                def2.resolve();
            });

            def.done(function () {

                setTimeout(function () {
                    this_.selectors.preloader.removeClass('is-active');
                    this_.selectors.introSection.addClass('is-active');
                    $('.intro-logo-ctn').addClass('is-active');
                }, 500);

                setTimeout(function () {
                    $('html').removeClass('is-disabled');
                }, 1000);
            });

        }

    },
    /**
     *  Fade in each image on individual project page as it comes into view
     */
    displayProjectImages: function () {

        var gridItem = $('.grid-img');

        gridItem.each(function () {
            $(this).bind('inview', function(event, isInView, visiblePartX, visiblePartY) {
                if (isInView) {
                    if (visiblePartY === 'top') {
                        $(this).addClass('is-active');
                        $(this).unbind();
                    } else {
                        $(this).addClass('is-active');
                        $(this).unbind();
                    }
                }
            });
        });

    },
    animateList: function( selector ) {

        var section = selector,
            item = $('.bullet-item');

        if(!Modernizr.touch){
            section.bind('inview', function(event, isInView) {
                if (isInView) {
                    item.each(function(i) {
                        $(this).delay((i++) * 80).fadeTo(100, 1);
                    });

                    section.unbind('inview');
                }
            });
        }else{
            item.each(function(index, element){
                $(element).css('opacity', 1);
            });
        }

    },
    footerAnim: function () {

        var this_ = this;

        this_.selectors.closingStripe.bind('inview', function(event, isInView) {

            if (isInView) {
                this_.selectors.closingStripe.addClass('is-active');
                this_.selectors.closingStripe.unbind('inview');
            }

        });

    },
    whichTransitionEvent: function(){
        var t;
        var el = document.createElement('fakeelement');
        var transitions = {
          'transition':'transitionend',
          'OTransition':'oTransitionEnd',
          'MozTransition':'transitionend',
          'WebkitTransition':'webkitTransitionEnd'
        };

        for(t in transitions){
            if( el.style[t] !== undefined ){
                return transitions[t];
            }
        }
    },
    init: function (){

        var this_ = this;

        //Cache Selectors
        this_.selectors = {};
        this_.selectors.mainHeader = $('.main-header');
        this_.selectors.mobNavToggle = $('.btn-toggle-nav');
        this_.selectors.mainNav = $('.main-nav');
        this_.selectors.mainHomeNav = $('.main-nav-home');
        this_.selectors.mainNavLinks = this_.selectors.mainNav.children();
        this_.selectors.introSection = $('.intro-section');
        this_.selectors.logoFader = $('.logo-fader');
        this_.selectors.grid = $('.grid');
        this_.selectors.map = $('#map-canvas');
        this_.selectors.closingStripe = $('.closing-stripe');
        this_.selectors.preloader = $('.preloader');

        //Store some info
        this_.stored = {};
        this_.stored.animationSelector = this_.detectBrowser();
        this_.stored.mainHeaderHeight = this_.selectors.mainHeader.height() - 1; //magic number 1 until investigated
        this_.stored.windowWidth = viewportSize.getWidth();
        this_.stored.windowHeight = viewportSize.getHeight();
        this_.stored.windowInner = window.innerHeight;
        this_.stored.transitionend = this_.whichTransitionEvent();

        //Set up some flags
        this_.flags = {};
        this_.flags.isScrolling = false;
        // this_.flags.isMobile = false;

        //Call methods

        //Process Images for retina displays and non mobile resolutions
        this_.processImages( isMobile );
        this_.processContentImages();

        this_.configureNavigation();


        //If IS homepage
        if( isHome ) {

            this_.preloadBackgroundImage();

            if(this_.stored.windowInner === 356){
                this_.stored.windowInner += 60;
            }

            this_.setRelativeUnits(this_.selectors.introSection, this_.stored.windowWidth, this_.stored.windowInner);
            this_.logoFader();
            this_.animateList( $('.news-section') );

            this_.selectors.introSection.bind('inview', function(event, isInView){
                if(!isInView){
                    $('.intro-logo-ctn').hide();
                }else{
                    $('.intro-logo-ctn').show();
                }
            });
        }

        //If IS homepage and ISNT touch
        if( !Modernizr.touch && isHome ) {
            this_.animateNav();
            this_.initiateParallax();
        }

        if( !Modernizr.touch && isProject ) {
            this_.initiateParallax();
        }

        //If IS work or IS individual project or IS about page
        if( isWork || isProject || isAbout || isPresentation) {
            this_.initialiseGrid();
        }

        this_.footerAnim();


        //Direction Arrows for individual project page
        if( !Modernizr.touch && isProject ) {
            $('.btn-ctn').on(this_.stored.transitionend, function(e)  {
                 $(e.target).removeClass('is-off');
            });

            $('.btn-project').on('mouseenter', function() {
                $(this).parents('.btn-ctn').removeClass('is-off').addClass('is-on');
            }).on('mouseleave', function() {
                $(this).parents('.btn-ctn').removeClass('is-on').addClass('is-off');
            });
        }

        /**
         *  Image Overlay
         */
        $('.image-popup').magnificPopup({
            type:'image',
            mainClass: 'mfp-with-zoom',
            callbacks: {
                open: function() {
                    $('.vimeo-player').each(function(index, element){
                        new Froogaloop(element).api('pause');
                    });

                    setTimeout(function () {
                        $('.mfp-figure').addClass('is-active');
                    }, 200);
                },
                close: function() {
                    $('.mfp-figure').removeClass('is-active');
                }
            },
            removalDelay: 300,
            closeMarkup: '<div class="mfp-close></div>'
        });

        /**
         *  Resize function keeps an eye on window height and window width
         *  if on homepage and relativeUnits aren't supported then the
         *  setRelativeUnits function is run
         */
        $(window).on('debouncedresize', function () {

            if( isHome ) {

                var ww = viewportSize.getWidth(),
                    wh = viewportSize.getHeight();

                    this_.setRelativeUnits(this_.selectors.introSection, ww, wh);
            }
        });

        (function(){

            enquire.register('screen and (min-width: 640px)', {
                match: function () {
                    this_.selectors.mainHeader.removeClass('is-active');
                }
            });

            var btnToggleNav = this_.selectors.mobNavToggle[0];

            if( Modernizr.touch){
                this_.selectors.mobNavToggle.on('touchend', function(e){
                    this_.selectors.mainHeader.toggleClass('is-active');
                    e.stopPropagation();

                });

                $('body').on('touchend', function(){
                    this_.selectors.mainHeader.removeClass('is-active');
                });

            }else{
                this_.selectors.mobNavToggle.on('click', function(e){
                    this_.selectors.mainHeader.toggleClass('is-active');
                    e.stopPropagation();

                });
                $('body').on('click', function(){
                    this_.selectors.mainHeader.removeClass('is-active');
                });
            }

            enquire.register('screen and (max-width:640px)', {
                match: function(){
                    this_.selectors.mainNav.css('display', 'none');
                    setTimeout(function(){
                        this_.selectors.mainNav.css('display', 'block');
                    }, 300);
                }
            });

            btnToggleNav.addEventListener('webkitTransitionEnd', function() {
                //$mainHeader.css('display', 'none'); setTimeout(function(){ $mainHeader.css('display', 'block'); });
            });

        })();

        //Prevent Awards being clicked if url is empty
        if( isProject ) {
            $('.disable-click').on('click', function(e) {
                e.preventDefault();
            });
        }
    }

};

//Start ------>
JL.init();

})();


(function() {
    //Youtube Video
    //Set up youTube iFrame API script
    var tag = document.createElement('script');
    tag.src = "//www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    var ytPlayers = [];
    var $vimeoPlayers;

    //Load thumbnails
    var youtubePlayerElements = $('.yt-video');

    youtubePlayerElements.each(function(index, element){
        var $element = $(element),
            $controls = $element.siblings('.video-controls'),
            ytid = $element.data('ytid');

        if($controls.children('img').length){
            return;
        }

        var request = $.ajax({
            url: 'http://gdata.youtube.com/feeds/api/videos/' + ytid + '?alt=jsonc&v=2',
            dataType: 'jsonp'
        });

        request.done(function(data){

            if(data.data.thumbnail.hqDefault){

                $controls.css('background-image', 'url(' + data.data.thumbnail.hqDefault + ')');
            }
        });
    });

    var vimeoPlayerElements = $('[data-source="vm"]');

    vimeoPlayerElements.each(function(index, element){

        var container = element.parentNode,
            $controls = $(container).children('.video-controls');
        var url = 'http://vimeo.com/api/v2/video/' + $(element).data('video-id') + '.json';

        if($controls.children('img').length){
            return;
        }

        var request = $.ajax({
            url: url,
            dataType: 'jsonp'
        });

        request.done(function(data){

            if(data['0'].thumbnail_large){

                var img = document.createElement('IMG');
                img.src = data['0'].thumbnail_large;
                $controls.prepend(img);
            }
        });
    });

    function initVimeos(){

        var $vimeos = $('[data-source="vm"]'),
            vimeoTemplate = jl.templates['js/templates/src/vimeo-player.hbs'],
            vimeoHeroTemplate = jl.templates['js/templates/src/hero-vimeo-player.hbs'];

        $vimeos.each(function(index, element){
            var data = {},
                $element = $(element);
            data.id = $element.data('video-id');

            if($element.data('type') === 'vimeo-hero'){
                $element.after(vimeoHeroTemplate(data));
            }else{
                $element.after(vimeoTemplate(data));
            }

            var player = $element.siblings('iframe')[0];

            new Froogaloop(player).addEvent('ready', ready);

            $element.remove();
        });

    }

    if(isHome){
        initVimeos();
    }else{
        $.subscribe('animationComplete', function() {
            initVimeos();
        });
    }

    window.onYouTubeIframeAPIReady = function() {
        if(isHome){
            this.myVideoManager = new VideoManager();
        }else{
            $.subscribe('animationComplete', function() {
                this.myVideoManager = new VideoManager();
            });
        }
    };

    function VideoManager() {
        this.$videos = $('.m-yt-ctn');
        this.players = [];

        for (var i = 0, count = this.$videos.length; i < count; i++) {
            var thisVideo = this.$videos[i];
            var currentYouTubeId = $(thisVideo).children('.yt-video').data('ytid');
            var player = new MyVideo(currentYouTubeId, thisVideo, this);
            this.players.push(player);
        }

        ytPlayers = this.players;
    }

    VideoManager.prototype.stopAll = function() {
        for (var i = 0, count = this.players.length; i < count; i++) {
            this.players[i].$controls.show().find('.btn-play-video').show();
            this.players[i].youtubePlayer.stopVideo();
        }
    };

    function MyVideo(youtubeId, videoContainer, videoManager) {
        this.videoManager = videoManager;
        this.$container = $(videoContainer);
        this.$video = this.$container.find('.yt-video');
        this.$controls = this.$container.children('.video-controls');


        this.youtubePlayer = new YT.Player( this.$video[0] , {
            width: '630px',
            height: '354px',
            videoId: youtubeId,
            title: '',
            playerVars: {
                modestbranding: 0,
                iv_load_policy: 3,
                showinfo: 0,
                autohide: 1,
                // controls: 0,
                wmode: 'transparent',
                origin : window.location.host,
                rel: 0
            },
            events: {
                'onReady': onReady,
                'onStateChange': onStateChange
            }
        });

        this.$container.append(this.controlsHTML);
        this.$controls = this.$container.find('.video-controls');

        this.$controls.on('click', '.btn-play-video', $.proxy(this.play, this));
        this.$controls.on('click', '.btn-pause-video', $.proxy(this.stop, this));
    }

    function onReady(e){
        setTimeout(function(){
            $(e.target.a).parent().children('.video-controls').remove();
        }, 100);
    }

    function onStateChange(state){
        if(state.data === 1){
            $vimeoPlayers.each(function(index, element){
                new Froogaloop(element).api('paused', function(value){
                    if(!value){
                        new Froogaloop(element).api('pause');
                    }
                });
            });
        }else if(state.data === 0){

            //$(state.target.a).parent().children('.video-controls').show();
        }
    }

    MyVideo.prototype.stop = function() {
        this.youtubePlayer.stopVideo();
        this.$controls.find('.btn-play-video').show();
    };

    MyVideo.prototype.play = function() {
        this.videoManager.stopAll();

        if(this.youtubePlayer.getPlayerState() === 2){
            this.youtubePlayer.pauseVideo();
        }else{
            this.youtubePlayer.playVideo();
        }

        //this.$controls.hide();//.end().find('.btn-play-video').hide();

        $vimeoPlayers.each(function(index, element){
            new Froogaloop(element).api('paused', function(value){
                if(!value){
                    new Froogaloop(element).api('pause');
                }
            });
        });
        // this.$controls.find('.btn-pause-video').show();
    };

    // Vimeo Video
    // Listen for the ready event for any vimeo video players on the page

    function onPlay(player_id){
        $.each(ytPlayers, function(index, element){
            if(element.youtubePlayer.getPlayerState() === 1){
                element.youtubePlayer.pauseVideo();
            }
            //element.$controls.show();
        });

        $vimeoPlayers.not('#' + player_id).each(function(index, element){
            new Froogaloop(element).api('paused', function(value){
                if(!value){
                    new Froogaloop(element).api('pause');
                }
            });
        });
    }

    // function onFinish(player_id){

    //     var element = document.getElementById(player_id),
    //         container = element.parentNode,
    //         $controls = $(container).children('.video-controls');

    // }

    function ready(player_id) {
        // Keep a reference to Froogaloop for this player
        var element = document.getElementById(player_id),
            container = element.parentNode,
            $controls = $(container).children('.video-controls');

        //Bit of a hack here. See JL-349 for details
        $(element).parents('.hero-section').css('-webkit-animation', 'none').css('-webkit-transform', 'translateY(0px)');

        $vimeoPlayers = $('.vimeo-player');

        new Froogaloop(element).addEvent('play', onPlay);
        //new Froogaloop(element).addEvent('finish', onFinish);

        setTimeout(function(){
            $controls.remove();
        }, 100);
    }

    //});

})();

// Key People Overlays

(function(){

    if(!$('.employee-section').length){
        return;
    }

    var $employeeSec = $('.employee-section'),
        $carousel = $('#employees-carousel'),
        $carouselHeader = $employeeSec.find('.employee-cta'),
        $people = $carousel.find('.employee');

    if(Modernizr.touch){
        $employeeSec.addClass('touch');
    }


    function isOnScreen(element){
        var offset = element.offset().top - $(window).height(),
            scrollTop = $(window).scrollTop();
        return (offset > scrollTop || scrollTop > offset + $(window).height()) ? false : true;
    }

    $(window).on('scroll', function(){
        if(isOnScreen($employeeSec)) {
            if($employeeSec.hasClass('visible')){
                return;
            }
            $employeeSec.removeClass('invisible').addClass('visible');
        }else{
            if($employeeSec.hasClass('invisible')){
                return;
            }
            $employeeSec.removeClass('visible').addClass('invisible');
            $people.removeClass('over');
            $people.find('.employee-overlay').removeClass('active');
            if( !Modernizr.touch && $(window).width() > 1020){
                $carouselHeader.css('display', 'block');
            }
        }
    });


    // Show Bio when "Read Bio" is clicked

    function showBio($overlay){

        $overlay.addClass('active');

        $overlay.children('.close-about').on('click touchend', function() {

            $(this).parent('.employee-overlay').removeClass('active');
            $(this).parents('.employee').removeClass('over');

            if(!$employeeSec.find('.active').length){
                $employeeSec.removeClass('explore');
            }

            $(this).off('click touchend');
        });
    }

    if(!Modernizr.touch){
        $employeeSec.on('mouseenter', function(){
            $carouselHeader.css('display', 'none');

        }).on('mouseleave', function(){

            if($(this).find('.active').length === 0 && $(window).width() > 1020){
                $carouselHeader.css('display', 'block');
            }
        });
    }

    if(!Modernizr.touch){
        $people.on('mouseenter', function(){
            $(this).addClass('over');
        }).on('mouseleave', function(){
            if(!$(this).find('.employee-overlay').hasClass('active')){
                $(this).removeClass('over');
            }
        });
    }else{

        $people.each(function(index, element){
            new Hammer(element).on('tap', function(e){
                e.preventDefault();


                $people.not(this).each(function(index, element){
                    if(!$(element).find('.employee-overlay').hasClass('active')){
                        $(element).removeClass('over');
                    }
                });

                if($(this).find('.employee-overlay').hasClass('active')){
                    return;
                }
                //$people.removeClass('over');
                $(this).toggleClass('over');
            });
        });

    }

    if(Modernizr.touch){
        $('.read-bio').each(function(index, element){
            new Hammer(element).on('tap', function(e){
                e.stopPropagation();
                showBio($(e.currentTarget).parents('.employee-overlay'));
                $employeeSec.addClass('explore');
            });
        });
    }else{
        $('.read-bio').on('click', function(e){
            showBio($(e.currentTarget).parents('.employee-overlay'));
            $employeeSec.addClass('explore');
        });
    }
})();





(function(){


    if(!$('#employee-section').length){
        return;
    }

    enquire.register('screen and (max-width: 1020px)', {
        match: function () {

            var $screenWidth = $(window).width(),
                $desktopBreakpoint = 1020,
                $tabletBreakpoint = 840,
                $mobileBreakpoint = 640,
                $carousel = $('#employees-carousel'),
                $slidesCtn = $carousel.find('.slides'),
                $slides = $slidesCtn.children(),
                $slidesNo = $slides.length,
                $pagination = $carousel.children('.carousel-pagination'),
                $paginationItems = $pagination.children(),
                $next = $carousel.parent().find('.next-slide'),
                $prev = $carousel.parent().find('.prev-slide'),
                $slidesShown,
                $moveLeft,
                $carouselWidth,
                $slideWidth;


            enquire.register('screen and (max-width: 640px)', {
                match: function () {
                    $resetSlider();
                }
            }).register('screen and (min-width: 641px) and (max-width: 840px)', {
                match: function () {
                    $resetSlider();
                }
            }).register('screen and (min-width: 841px) and (max-width: 1020px)', {
                match: function () {
                    $resetSlider();
                }
            }).register('screen and (min-width: 1021px)', {
                match: function () {
                    $resetSlider();
                }
            }, true);

            var $layoutMath = function(){
                if ($screenWidth <= $mobileBreakpoint) {
                    $slidesShown = 1;
                }else if ($screenWidth <= $tabletBreakpoint) {
                    $slidesShown = 2;
                }else if ($screenWidth <= $desktopBreakpoint) {
                    $slidesShown = 3;
                }else{
                    $slidesShown = 5;
                }
                return $slidesShown;
            };

            $layoutMath();

            var initialisePagination = function(){
                $paginationItems.removeClass('current');
                for(var i = $currentSlide, j = $currentSlide + $slidesShown; i < j; i++){
                    $paginationItems.eq(i).addClass('current');
                }
            };

            var $resizefn = function(){
                $carouselWidth = $carousel.width();
                $slideWidth = Math.ceil($carouselWidth / $slidesShown);
                $carousel.height(Math.floor($carouselWidth / $slidesShown));
                initialisePagination();

                $slides.css('width', $slideWidth);
                $slidesCtn.css('width', ($slideWidth * $slidesNo));

                //Temp: used to hide nav and carousel while there are only three Key People.
                if($slidesShown < $slidesNo){
                    $showNav();
                    $pagination.show();
                }else{
                    $pagination.hide();
                }
            };

            function $showNav(){
                if( !Modernizr.touch ){
                    $next.addClass('show-nav');
                    $prev.addClass('show-nav disabled');
                }
            }

            var transforms = ['transform', '-webkit-transform', '-moz-transform'],
                CSSTransform,
                transEl = document.createElement('div');

            for(var i in transforms){
                if(transforms[i] in transEl.style){
                    CSSTransform = transforms[i];
                }
            }

            $slidesCtn.css(CSSTransform, 'translateX(0px)');

            function $resetSlider() {
                if (CSSTransform) {
                    $slidesCtn.css(CSSTransform, 'translateX(0px)');
                } else {
                    $slidesCtn.css('left', 0);
                }
                $currentSlide = 0;
            }


            $resizefn();

            var $incrementMax = $slidesNo - $slidesShown;


            var $currentSlide = 0;

            var $carouselNav = function(){
                if ($currentSlide === $incrementMax){
                    $next.addClass('disabled');
                }else if($currentSlide < $incrementMax){
                    $next.removeClass('disabled');
                }

                if($currentSlide === 0){
                    $prev.addClass('disabled');
                }
            };

            var $nextSlide = function(){
                if ($currentSlide < $incrementMax) {
                    $currentSlide++;

                    $paginationItems.removeClass('current');

                    for(var i = $currentSlide, j = $currentSlide + $slidesShown; i < j; i++){
                        $paginationItems.eq(i).addClass('current');
                    }

                    $moveLeft = $slideWidth * -($currentSlide);
                    if (CSSTransform) {
                        $slidesCtn.css(CSSTransform, 'translateX('+ Math.ceil($moveLeft) +'px)');
                    } else {
                        $slidesCtn.css('left', Math.ceil($moveLeft));
                    }
                    $prev.removeClass('disabled');
                    $carouselNav();
                }
            };

            var $prevSlide = function(){
                if ($currentSlide > 0) {
                    $currentSlide--;

                    $paginationItems.removeClass('current');

                    for(var i = $currentSlide, j = $currentSlide + $slidesShown; i < j; i++){
                        $paginationItems.eq(i).addClass('current');
                    }

                    $moveLeft = $slideWidth * -($currentSlide);
                    if (CSSTransform) {
                        $slidesCtn.css(CSSTransform, 'translateX('+ Math.floor($moveLeft) +'px)');
                    } else {
                        $slidesCtn.css('left', Math.ceil($moveLeft));
                    }
                    $carouselNav();
                }
            };

            if( !Modernizr.touch ){

                $next.on('click', function(){
                    $nextSlide();
                });

                $prev.on('click', function() {
                    $prevSlide();
                });
            }else{

                var $employeeSec = $('.employee-section'),
                    initPos,
                    currentPos,
                    currentOffset,
                    currentTransfrom,
                    offset,
                    startX,
                    startY;

                $employeeSec.on('touchstart', function(e){
                    console.log('touchstart');
                    $slidesCtn.addClass('no-transition');
                    currentTransfrom = $slidesCtn.css(CSSTransform);
                    currentOffset = parseInt(currentTransfrom.substr(7, currentTransfrom.length - 8).split(', ')[4], 10);
                    initPos = e.originalEvent.touches[0].pageX;
                    currentPos = e.originalEvent.touches[0].pageX;
                    offset = currentPos - initPos;
                    startX = e.originalEvent.touches[0].pageX;
                    startY = e.originalEvent.touches[0].pageY;
                });

                $employeeSec.on('touchmove', function(e){
                    // if($slidesShown === 5){
                    //     return;
                    // }

                    if($('#employee-section').hasClass('vertical')){
                        //return;
                     }

                    if(Math.abs(startX - e.originalEvent.touches[0].pageX) > Math.abs(startY - e.originalEvent.touches[0].pageY)){
                        console.log('horizontal');
                        e.preventDefault();
                        $('#employee-section').addClass('horizontal');

                        currentPos = e.originalEvent.touches[0].pageX;
                        offset = currentPos - initPos;
                        //initPos = currentPos;
                        $slidesCtn.css(CSSTransform, 'translateX('+ (currentOffset + offset) +'px)');
                    }else{
                        console.log('vertical');
                        //$(this).off('touchmove');
                        $('#employee-section').addClass('vertical');
                    }


                });

                $employeeSec.on('touchend', function(){
                    console.log('touchend');
                    $('#employee-section').removeClass('horizontal').removeClass('vertical');
                    $slidesCtn.removeClass('no-transition');
                    currentOffset += offset;
                    var offsetNo = Math.round(currentOffset / $slideWidth),
                        limit = $slidesNo - $slidesShown;

                    if(offsetNo < -limit){
                        offsetNo = -limit;
                    }else if(offsetNo > 0){
                        offsetNo = 0;
                    }
                    $slidesCtn.css(CSSTransform, 'translateX(' + offsetNo * $slideWidth + 'px)');

                    $currentSlide = Math.abs(offsetNo);

                    $paginationItems.removeClass('current');
                    for(var i = $currentSlide, j = $currentSlide + $slidesShown; i < j; i++){
                        $paginationItems.eq(i).addClass('current');
                    }
                });

            }


            $(window).on('debouncedresize', function(){
                $screenWidth = $(window).width();
                $slidesShown = $layoutMath();
                $incrementMax = $slidesNo - $slidesShown;
                $carousel.height(Math.floor($screenWidth / $slidesShown));
                $resizefn();
                initialisePagination();


                if($screenWidth > 1020){

                    $next.removeClass('show-nav');
                    $prev.removeClass('show-nav');
                }

                var $slidesCtnPos = $slidesCtn.css('left'),
                    $slidesLeft;

                if($slidesCtnPos !== "0px"){
                    $slidesLeft = $slideWidth + parseFloat($slidesCtnPos);
                    $slidesCtnPos = (parseFloat($slidesCtnPos) - $slidesLeft) * $currentSlide;
                    $slidesCtn.css('left', $slidesCtnPos);
                }

            });
        }
    }, true);




})();


(function(){

    if(!$('.job-carousel').length){
        return;
    }

    var $screenWidth = $(window).width(),
        $breakMargin = 0,
        $carousel = $('.job-carousel'),
        $slidesCtn = $carousel.find('.slides'),
        $slides = $slidesCtn.children(),
        $slidesNo = $slides.length,
        $slidesShown,
        $pagesNo,
        $pagination = $carousel.find('.carousel-pagination'),
        // $paginationItems = $pagination.children(),
        $next = $carousel.parent().find('.next-slide'),
        $prev = $carousel.parent().find('.prev-slide'),
        // $slidesShown = $layoutMath(),
        $moveLeft,
        $carouselWidth,
        $slideWidth,
        $slideFullWidth,
        $incrementMax,
        $currentSlide = 0;


    enquire.register('screen and (max-width: 640px)', {
        match: function () {
            $slidesShown = 1;
            $breakMargin = 0;

            $atBreakpoint();
        }
    }).register('screen and (min-width: 641px) and (max-width: 840px)', {
        match: function () {
            $slidesShown = 2;
            $breakMargin = 50;

            $atBreakpoint();
        }
    }).register('screen and (min-width: 841px)', {
        match: function () {
            $slidesShown = 3;
            $breakMargin = 50;

            $atBreakpoint();
        }
    }, true);

    function $atBreakpoint(){
        $resetSlider();
        $setPagination();
        $updatePagination();
    }

    function $resetSlider() {
        $slidesCtn.css('left', 0);
        $currentSlide = 0;
        $prev.addClass('disabled');
        $next.removeClass('disabled');
    }

    function $setPagination(){
        if ($pagination) {

            $pagination.empty();
            $next.removeClass('show-nav');
            $prev.removeClass('show-nav');

            $pagesNo = Math.ceil($slidesNo / $slidesShown);

            if ($pagesNo > 1) {
                for (var $i = 1; $i <= $pagesNo; $i++) {
                    $pagination.append("<span class='pagination-item'></span>");
                }
                if( !Modernizr.touch ){

                    $showNav();
                }
            }
        }
    }

    function $updatePagination(){
        var $currentPage = $currentSlide +1;
        var $paginationItems = $pagination.children();
        if($paginationItems.hasClass('current')){
            $paginationItems.removeClass('current');
        }
        $pagination.find('span:nth-child(' + $currentPage + ')').addClass('current');
    }

    function $resizefn(){
        $carouselWidth = $carousel.width();
        $carouselWidth = $carouselWidth + $breakMargin;
        $slideWidth = ($carouselWidth / $slidesShown) - $breakMargin;
        $slideFullWidth = $slideWidth + $breakMargin;
        $slides.css({
            width : $slideWidth,
            'margin-right' : $breakMargin
        });
        $slidesCtn.css('width', (($slideWidth+$breakMargin) * $slidesNo));
    }

    function $showNav(){
       if(!Modernizr.touch){
            $next.addClass('show-nav');
            $prev.addClass('show-nav disabled');
        }
    }

    /**
     *  Determines the direction the carousel slides in
     *  Direction is determined and the slider left position animated + or -
     *  @param dir string
     */
    function slide(dir) {

        if (dir === 'next' && $currentSlide < $incrementMax) {
            ++$currentSlide;
        } else if(dir === 'previous' && $currentSlide > 0) {
            --$currentSlide;
        }

        var counter = $slidesShown * $currentSlide;

        $moveLeft = $slideFullWidth * counter;

        $slidesCtn.css('left', -$moveLeft);

        $updatePagination();

        if( dir === 'next' ) {
            $prev.removeClass('disabled');
        }

        $carouselNav( $currentSlide );
    }

    /**
     *  Manages the carousel navigation state - active/disabled
     */
    function $carouselNav(slide){

        if ( slide === $incrementMax ){
            $next.addClass('disabled');
        }else if( slide < $incrementMax ){
            $next.removeClass('disabled');
        }

        if( slide === 0 ){
            $prev.addClass('disabled');
        }
    }

    /**
     *  Return a value that represents remaining items left in carousel excluding items already in view
     */
    function incrementMax() {
        $incrementMax = Math.ceil(($slidesNo - $slidesShown) / $slidesShown);
        return $incrementMax;
    }


    if($slidesNo > 1){

        $resizefn();

        $incrementMax = incrementMax();

        if($pagesNo > $slidesShown){
            $setPagination();
            $updatePagination();
        }

        $(window).on('debouncedresize', function(){
            $screenWidth = $(window).width();

            $incrementMax = incrementMax();

            $resizefn();

            if($pagesNo > $slidesShown){
                $showNav();
            }

            var $slidesCtnPos = $slidesCtn.css('left'),
                $slidesLeft;

            if($slidesCtnPos !== "0px"){
                $slidesLeft = $slideWidth + parseFloat($slidesCtnPos);
                $slidesCtnPos = (parseFloat($slidesCtnPos) - $slidesLeft) * $currentSlide;
                $slidesCtn.css('left', $slidesCtnPos);
            }

        });

        if(!Modernizr.touch){
            $next.on('click', function(){
                slide('next');
            });

            $prev.on('click', function(){
                slide('previous');
            });

        }else{

            var hammerCarousel = new Hammer(document.getElementById('job-section'), {
                drag_min_distance: 50
            });

            hammerCarousel.on('drag', function(e) {
                if($('.job-section').hasClass('scrolling')){
                    return;
                }
                if(e.gesture.direction === 'left' || e.gesture.direction === 'right'){
                    e.gesture.preventDefault();
                    $('.job-section').addClass('scrolling').addClass('horizontal');
                }else{
                    $('.job-section').addClass('scrolling').addClass('vertical');
                }
            });

            hammerCarousel.on('dragend', function(){
                $('.job-section').removeClass('scrolling').removeClass('horizontal').removeClass('vertical');
            });

            hammerCarousel.on('swipeleft', function(){
                if($('.job-section').hasClass('horizontal')){
                    slide('next');
                }

            });

            hammerCarousel.on('swiperight', function(){
                if($('.job-section').hasClass('horizontal')){
                    slide('previous');
                }
            });
        }
    }
})();

(function(){
    var $peopleGrid = $('.people-grid'),
        $people = $peopleGrid.find('.employee');

    if(!$peopleGrid.length){
        return;
    }

    $people.imagesLoaded(function(){
        $('.preloader').removeClass('is-active');
        $peopleGrid.addClass('show');
    });

    if(Modernizr.touch){
        $people.each(function(index, element){
            new Hammer(element).on('tap', function(){
                $people.removeClass('active');
                $(this).addClass('active');
            });
        });

    }else{
        $people.on('mouseenter', function(){
            $(this).addClass('active');
        }).on('mouseleave', function(){
            $(this).removeClass('active');
        });
    }
})();
