// Include Gulp.
var gulp = require('gulp');

// Include Plugins.
var sass = require('gulp-ruby-sass'),
    jshint = require('gulp-jshint'),
    uglify = require('gulp-uglify'),
    imagemin = require('gulp-imagemin'),
    concat = require('gulp-concat'),
    rename = require('gulp-rename'),
    handlebars = require('gulp-handlebars'),
    notify = require('gulp-notify'),
    livereload = require('gulp-livereload'),
    lr = require('tiny-lr'),
    browserify = require('gulp-browserify'),
    server = lr();


// SASS + Compass Tasks.
gulp.task('compass', function() {
  return gulp.src('./css/src/base.scss')
    .pipe(sass({ style: 'compressed', compass: true }))
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest('./css'))
    .pipe(livereload(server))
    .pipe(notify({ message: 'Compass task complete.' }));
});


// Scripts Task.
gulp.task('scripts', function() {
    return gulp.src([
        './js/libs/*/*.js',
        './js/src/**/*.js'
    ])
    .pipe(browserify())
    .pipe(concat('main.concat.js'))
    .pipe(gulp.dest('./js'))
    .pipe(rename('main.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./js'))
    .pipe(livereload(server))
    .pipe(notify({ message: 'Scripts task complete.' }));
});


// Compress Images Task.
gulp.task('imagemin', function() {
  return gulp.src('./img/**')
    .pipe(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true }))
    .pipe(gulp.dest('./img'))
    .pipe(livereload(server))
    .pipe(notify({ message: 'Images task complete.' }));
});


// Template Task.
gulp.task('templates', function(){
    gulp.src([
        './templates/*.hbs'
    ])
    .pipe(handlebars({
        outputType: 'node'
    }))
    .pipe(concat('templates.js'))
    .pipe(gulp.dest('./js/src/'))
    .pipe(livereload(server))
    // .pipe(notify({ message: 'Templates task complete.' }));
});


// Watch Task.
gulp.task('watch', function() {
    gulp.watch('./js/*/*.js', ['scripts']);
    gulp.watch('./css/src/**/*.scss', ['compass']);
    gulp.watch('./templates/*.hbs', ['templates']);
});


// Default Tasks.
gulp.task('default', ['compass', 'scripts', 'watch']);